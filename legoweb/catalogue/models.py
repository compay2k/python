from django.db import models


class Color(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=6)

    def __str__(self):
        return self.name + "(#" + self.value + ")"


class Brick(models.Model):
    reference = models.CharField(max_length=50)
    color = models.ForeignKey(Color, on_delete=models.PROTECT)
    width = models.IntegerField(default=1)
    length = models.IntegerField(default=1)
    height = models.IntegerField(default=1)
    price = models.FloatField(default=0)


class Cart(models.Model):
    creation_date = models.DateTimeField(default="2022-08-22 06:42:00")

class CartItem(models.Model):
    brick = models.ForeignKey(Brick, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)



# https://docs.djangoproject.com/fr/4.0/intro/tutorial02/
