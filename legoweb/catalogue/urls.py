from django.urls import include, path

from . import views
from rest_framework import routers
from .views import *

app_name = 'catalogue'

router = routers.SimpleRouter()
router.register("color", ColorViewSet, basename="color")
router.register("brick", BrickViewSet, basename="brick")
router.register("cart", CartViewSet, basename="cart")
router.register("cart-item", CartItemViewSet, basename="cart-item")

urlpatterns = [
    path("", views.index, name="index"),
    path("colors/<int:color_id>/", views.bricks_by_color, name="colors"),
    path("cart/", views.add_bricks, name="add_bricks"),
    path("api/", include(router.urls)),
]
