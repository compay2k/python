from django.http import Http404
from django.shortcuts import render, get_object_or_404
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

# from django.template import loader

from .models import Brick, Color, CartItem
from .serializers import *

def index(request):
    colors_list = Color.objects.all()
    
    context = {
        'colors_list': colors_list,
    }
    
    # template = loader.get_template('catalogue/index.html')
    # return HttpResponse(template.render(context, request))
    return render(request, 'catalogue/index.html', context)

def bricks_by_color(request, color_id):
    
    #try:
    #    color = Color.objects.get(id=color_id)
    #except Color.DoesNotExist:
    #    raise Http404("Color %s does not exist" %color_id)
    
    color = get_object_or_404(Color, id=color_id)
    bricks = Brick.objects.filter(color = color_id)    
    
    return render(request, 'catalogue/bricks.html', { 'color': color, 'bricks': bricks })


def add_bricks(request):

    cart_items = []

    for param in request.POST:
        if (param.startswith("quantity")):
            brick_id = param[8:]
            item = CartItem(brick=Brick.objects.get(id=int(brick_id)), quantity=request.POST[param])
            cart_items.append(item)

    return render(request, 'catalogue/cart.html', { 'cart_items': cart_items })


class ColorViewSet(ModelViewSet):

    serializer_class = ColorSerializer

    def get_queryset(self):
        return Color.objects.all()

class BrickViewSet(ModelViewSet):

    serializer_class = BrickSerializer

    def get_queryset(self):
        return Brick.objects.all()

class CartViewSet(ModelViewSet):

    serializer_class = CartSerializer

    def get_queryset(self):
        return Cart.objects.all()

class CartItemViewSet(ModelViewSet):

    serializer_class = CartItemSerializer

    def get_queryset(self):
        return CartItem.objects.all()

    def create(self, request):
       
        item = CartItem()
            
        item.brick_id = request.data["brick"]
        item.cart_id = request.data["cart"]
        item.quantity = int(request.data["quantity"])
        item.quantity += 10

        # TODO : implement buiness logic

        item.save()

        return Response(CartItemSerializer(item).data)
