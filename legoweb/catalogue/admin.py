from django.contrib import admin

from .models import Brick, Color

admin.site.register([Color, Brick])