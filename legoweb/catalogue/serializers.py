from rest_framework.serializers import ModelSerializer

from catalogue.models import Color, Brick, Cart, CartItem


class ColorSerializer(ModelSerializer):
    class Meta:
        model = Color
        fields = ["id", "name", "value"]

class BrickSerializer(ModelSerializer):
    class Meta:
        model = Brick
        fields = ["id", "reference", "color", "width", "length", "height", "price"]

class CartSerializer(ModelSerializer):
    class Meta:
        model = Cart
        fields = ["id", "creation_date"]


class CartItemSerializer(ModelSerializer):
    class Meta:
        model = CartItem
        fields = ["id", "brick", "quantity", "cart"]

