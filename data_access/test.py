import psycopg2
from configparser import ConfigParser


def config(filename="database.ini", section="postgresql"):
    parser = ConfigParser()
    parser.read(filename)

    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception(
            "Section {0} not found in the {1} file".format(section, filename)
        )

    return db


def connect():
    cnx = None

    try:
        params = config()
        print("connecting to db...")
        cnx = psycopg2.connect(**params)
        cur = cnx.cursor()
        print("pgsql version : ")
        cur.execute("SELECT version()")

        db_version = cur.fetchone()
        print(db_version)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if cnx is not None:
            cnx.close()
            print("Database connection closed.")


def create_tables():
    commands = (
        """
        DROP TABLE IF EXISTS lego_set
        """,
        """
        CREATE TABLE lego_set (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            reference VARCHAR(255) NOT NULL,
            nb_pieces INTEGER,
            initial_price NUMERIC(5,2)
        )
        """,
    )

    cnx = None

    try:
        params = config()
        cnx = psycopg2.connect(**params)
        cur = cnx.cursor()
        for command in commands:
            cur.execute(command)
        cur.close()
        cnx.commit()

        print("tables created")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if cnx is not None:
            cnx.close()


def insert_data():
    command = """
        INSERT INTO lego_set(name, reference, nb_pieces, initial_price) VALUES ('plop', 'ref1234', 42, 99.99)
        """

    cnx = None

    try:
        params = config()
        cnx = psycopg2.connect(**params)
        cur = cnx.cursor()
        cur.execute(command)
        cur.close()
        cnx.commit()

        print("row inserted")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if cnx is not None:
            cnx.close()


# connect()
create_tables()
insert_data()
