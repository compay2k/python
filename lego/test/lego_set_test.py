import pytest 

from lego.lego_set import LegoSet

def test_price_per_piece():
    l = LegoSet("plop", "12345", 100, 100)
    assert l.price_per_piece() == 1


def test_initial_price_is_positice():
    with pytest.raises(ValueError):    
        l = LegoSet("plop", "12345", 100, 0)
        
