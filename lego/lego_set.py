# from attr import attrs, attrib

class LegoSet:
    def __init__(self, name: str, reference: str, nb_pieces: int, intial_price: float):
        self._name = name
        self._reference = reference
        self._nb_pieces = nb_pieces
        self.initial_price = intial_price

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def initial_price(self):
        return self._initial_price

    @initial_price.setter
    def initial_price(self, initial_price):
        if initial_price > 0:
            self._initial_price = initial_price
        else:
            raise ValueError("pas bien")

    def price_per_piece(self) -> float:
        return self.initial_price / self._nb_pieces

    def __str__(self) -> str:
        return (
            "Set #"
            + self._reference
            + " - "
            + self._name
            + " - "
            + str(self._nb_pieces)
            + " pces - "
            + str(self._initial_price)
            + " €"
        )


