import sys

from lego.lego_set import LegoSet

def main() -> int:
    my_set = LegoSet("Stormtrooper", "75276", 785, 60)
    print(my_set)
    return 0

if __name__ == "__main__":
        sys.exit(main())

