import unittest
from day1 import Day1

class TestDay1(unittest.TestCase):

	def setUp(self):
		data = '1721\r\n979\r\n366\r\n299\r\n675\r\n1456'
		self.sut = Day1(data)	

	def test_correct_pair(self):
		self.sut.proceedSumOfTwo()
		self.sut.displayResult()
		self.assertEqual(self.sut.first + self.sut.second, 2020)

	def test_correct_trio(self):
		self.sut.proceedSumOfThree()
		self.sut.displayResult()
		self.assertEqual(self.sut.first + self.sut.second + self.sut.third, 2020)

if __name__ == '__main__':
	unittest.main()