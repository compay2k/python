class Day1:

	def __init__(self, data):
		self.SEEK_VALUE = 2020
		self.data = data.split('\n')
		self.first = 0
		self.second = 0
		self.third = 0

	def proceedSumOfTwo(self):
		self.third = 0
		for i in range(0, len(self.data)):
			for j in range(i, len(self.data)):
				number1 = int(self.data[i])
				number2 = int(self.data[j])
				
				if number1 + number2 == self.SEEK_VALUE:
					self.first = number1
					self.second = number2
					return

	def proceedSumOfThree(self):
		for i in range(0, len(self.data)):
			for j in range(i, len(self.data)):
				for k in range(j, len(self.data)):
					number1 = int(self.data[i])
					number2 = int(self.data[j])
					number3 = int(self.data[k])
					
					if number1 + number2 + number3 == self.SEEK_VALUE:
						self.first = number1
						self.second = number2
						self.third = number3
						return


	def displayResult(self):
		print("values are " + str(self.first) + " and " + str(self.second) + " and " + str(self.third))
		print("product is " + str(self.first * self.second * self.third))



if __name__ == '__main__':
	f = open("day1Data.txt", "r")
	day1 = Day1(f.read())
	day1.proceedSumOfTwo()
	day1.displayResult()
	print('------')
	day1.proceedSumOfThree()
	day1.displayResult()
	