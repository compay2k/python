import unittest
from day3 import Day3

class TestDay3(unittest.TestCase):

	def setUp(self) :
		f = open("day3TestData.txt", "r")
		data = f.read()
		f.close()
		self.day3 = Day3(data)

	def test_move(self):
		self.day3.move(3,1)
		self.assertEqual(self.day3.trees, 7)

	def test_step2(self):
		self.assertEqual(self.day3.multipleSteps([[1,1],[3,1],[5,1],[7,1],[1,2]]) , 336)

if __name__ == '__main__':
	unittest.main()