class Day3:

	def __init__(self, data):
		self.pattern = data.splitlines()
		self.trees = 0
		self.currentLine = 0
		self.currentColumn = 0

	def multipleSteps(self, moves):
		result = 1
		for line in moves:
			result *= self.move(line[0], line[1])
		return result 

	def move(self, right, down):
		self.trees = 0
		self.currentLine = 0
		self.currentColumn = 0
		return self.step(right, down)	

	def step(self, right, down):
		if self.currentLine < len(self.pattern) - 1:
			self.currentLine += down
			self.currentColumn += right
			lineWidth = len(self.pattern[self.currentLine])
			square = self.pattern[self.currentLine][self.currentColumn % lineWidth]
			if square == "#" : self.trees += 1
			self.step(right, down)
		return self.trees

if __name__ == '__main__':
	f = open("day3Data.txt", "r")
	data = f.read()
	f.close()
	day3 = Day3(data)
	print(day3.move(3, 1))
	print(day3.multipleSteps([[1,1],[3,1],[5,1],[7,1],[1,2]]))



