import unittest
from day2 import Day2, Credential, CredentialV1, CredentialV2

class TestDay2(unittest.TestCase):

	def setUp(self):
		data = '1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc'
		self.sutv1 = Day2(data, "v1")	
		self.sutv2 = Day2(data, "v2")
	
	def test_number_of_credentials(self):
		self.assertEqual(self.sutv1.credentialsCount, 3)

	def test_number_of_correct_credentialsV1(self):
		self.assertEqual(self.sutv1.correctCredentialsCount, 2)

	def test_correct_passwordV1(self):
		credential = CredentialV1('1-3 a: abcde')
		print(credential)
		self.assertTrue(credential.isCorrect())	

	def test_incorrect_passwordV1(self):
		credential = CredentialV1('1-3 b: cdefg')
		print(credential)
		self.assertFalse(credential.isCorrect())

	def test_number_of_correct_credentialsV2(self):
		self.assertEqual(self.sutv2.correctCredentialsCount, 1)

	def test_correct_passwordV2(self):
		credential = CredentialV2('1-3 a: abcde')
		print(credential)
		self.assertTrue(credential.isCorrect())	

	def test_incorrect_passwordV2(self):
		credential = CredentialV2('1-3 b: cdefg')
		print(credential)
		self.assertFalse(credential.isCorrect())	



if __name__ == '__main__':
	unittest.main()