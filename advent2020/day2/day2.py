from abc import ABC, abstractmethod

class Day2:

	def __init__(self, data, version):
		lines = data.split("\n")
		self.credentials = []
		self.version = version
		for line in lines:
			self.credentials.append(self.createCredential(line))

	def createCredential(self, line):
		if self.version == "v1": 
			return CredentialV1(line)
		elif self.version == "v2": 
			return CredentialV2(line)
		else:
			return None

	@property
	def credentialsCount(self):
		return len(self.credentials)
	
	@property
	def correctCredentialsCount(self):
		correct = 0
		for credential in self.credentials:
			if credential.isCorrect():correct+=1
		return correct

class Credential(ABC):
	
	def __init__(self, line):
		values = line.split(': ')
		policyValues = values[0].split(' ')
		policyOccurences = policyValues[0].split('-')

		self.bound1 = int(policyOccurences[0])
		self.bound2 = int(policyOccurences[1])
		self.requiredLetter = policyValues[1]	
		self.password = values[1]

	
	@abstractmethod
	def isCorrect(self):
		pass


class CredentialV1(Credential):

	def isCorrect(self):
		occurences = self.password.count(self.requiredLetter)
		return occurences >= self.bound1 and occurences <= self.bound2

	def __repr__(self):
			return self.requiredLetter + " occurs " + str(self.bound1) + "-" + str(self.bound2) + " times - Password : " + self.password

class CredentialV2(Credential):

	def isCorrect(self):
		return (self.password[self.bound1 - 1] == self.requiredLetter) !=  (self.password[self.bound2 - 1] == self.requiredLetter)

	def __repr__(self):
			return "exactly one of positions " + str(self.bound1) + " and " + str(self.bound2) + " must contain [" + self.requiredLetter + "] - Password : " + self.password


if __name__ == '__main__':
	f = open("day2Data.txt", "r")
	data = f.read()
	day2 = Day2(data, "v1")
	print(day2.correctCredentialsCount)
	day2 = Day2(data, "v2")
	print(day2.correctCredentialsCount)




