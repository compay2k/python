import unittest
from day4 import Day4, Passport

class TestDay3(unittest.TestCase):

	def setUp(self) :
		f = open("day4TestData.txt", "r")
		data = f.read()
		f.close()
		self.day4 = Day4(data)
	
	def testFormatting(self):
		rawData = "hgt:176cm\niyr:2013\nhcl:#fffffd ecl:amb\nbyr:2000\neyr:2034\ncid:89 pid:934693255"
		expectedData = "hgt:176cm iyr:2013 hcl:#fffffd ecl:amb byr:2000 eyr:2034 cid:89 pid:934693255"
		passport = Passport(rawData)
		rawData = passport.format(rawData)
		self.assertEqual(rawData, expectedData)
	
	def testValidPassports(self):
		self.assertEqual(self.day4.validPassports, 2)



if __name__ == '__main__':
	unittest.main()