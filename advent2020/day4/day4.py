from abc import ABC, abstractmethod
import re

class Day4:

	def __init__(self, data):
		passportsList = data.split("\n\n")
		self.passports = []
		self.validPassports = 0

		for passportData in passportsList :
			passport = Passport(passportData)
			self.passports.append(passport)
			if passport.isValid():
				self.validPassports += 1


class Passport:

	def __init__(self, data):

		data = self.format(data)
		fieldList = data.split(" ")

		self.fields = []

		self.fields.append(self.createField("byr", fieldList, True, RangeValidator(4, 1920, 2002))) # (Birth Year)
		self.fields.append(self.createField("iyr", fieldList, True, RangeValidator(4, 2010, 2020))) # (Issue Year)
		self.fields.append(self.createField("eyr", fieldList, True, RangeValidator(4, 2020, 2030))) # (Expiration Year)
		self.fields.append(self.createField("hgt", fieldList, True, UnitValidator())) # (Height)
		self.fields.append(self.createField("hcl", fieldList, True, RegExpValidator("^#[0-9a-f]{6}$"))) # (Hair Color)
		self.fields.append(self.createField("ecl", fieldList, True, RegExpValidator("amb|blu|brn|gry|grn|hzl|oth"))) # (Eye Color)
		self.fields.append(self.createField("pid", fieldList, True, RegExpValidator("^\d{9}$"))) # (Passport ID)
		self.fields.append(self.createField("cid", fieldList, False, AlwaysOKValidator())) # (Country ID)


	def createField(self, name, fieldList, isMandatory, validator):
		for field in fieldList:
			nameValuePair = field.split(":")
			if name == nameValuePair[0]:
				value = nameValuePair[1]
				return Field(name, value, isMandatory, validator)		
		return Field(name, "N/C", isMandatory, validator)

	def format(self, data):
		return data.replace("\n", " ")

	def isValid(self):
		for field in self.fields:
			if field.isValid() == False:
				return False
		return True	

class Field:

	def __init__(self, name, value, isMandatory, validator):
		self.name = name
		self.value = value
		self.isMandatory = isMandatory
		self.validator = validator

	def isValid(self):
		if self.isMandatory == True and self.value == "N/C":
			return False
		if self.validator.validate(self) == False:
			return False
		return True

class Validator(ABC):
	@abstractmethod
	def validate(self, field):
		pass
	
class RangeValidator(Validator):

	def __init__(self, nbDigits, min, max):
		self.nbDigits = nbDigits
		self.min = min
		self.max = max

	def validate(self, field):
		
		return re.search("^\d{" + str(self.nbDigits) + "}$", field.value) != None \
			and int(field.value) >= self.min \
			and int(field.value) <= self.max

class UnitValidator(Validator):		
		
	def validate(self, field):
		if re.search( "cm|in$", field.value) == None: 
			return False
		
		min = -1
		max = -1
		
		unitLength = 2
		
		unit = field.value[len(field.value) - unitLength: len(field.value)]
		
		if (unit == "cm"):
			min = 150
			max = 193
		
		if (unit == "in"):
			min = 59
			max = 76

		value = int(field.value[0:len(field.value)-unitLength])

		return value >= min \
			and value <= max \
			and (re.search("^cm|in$", unit)) != None 
			
class RegExpValidator(Validator):

	def __init__(self, regExp):
		self.regExp = regExp

	def validate(self, field):
		return re.search(self.regExp, field.value) != None

class AlwaysOKValidator(Validator):

	def validate(self, field):
		return True


if __name__ == '__main__':
	f = open("day4Data.txt", "r")
	data = f.read()
	f.close()
	day4 = Day4(data)
	print(day4.validPassports)